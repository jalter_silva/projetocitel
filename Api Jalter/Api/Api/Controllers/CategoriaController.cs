﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Provider.Models;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<Categoria>>> GetCategoria()
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.GetCategoria();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Categoria>> Get(int id)
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.GetCategoria(id);
        }

        [HttpGet("CategoriaCombo")]
        public async Task<ActionResult<List<CategoriaViewModel>>> CategoriaCombo()
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.CategoriaCombo();
        }

        // POST api/values
        [HttpPost]
        public async Task<Categoria> Post([FromBody] Categoria categoria)
        {
            Provider.Provider provider = new Provider.Provider();

            categoria = await provider.InsertCategory(categoria);

            return categoria;
        }

        [HttpPut]
        public async Task<Categoria> Put([FromBody] Categoria categoria)
        {
            Provider.Provider provider = new Provider.Provider();

            categoria = await provider.PutCategory(categoria);

            return categoria;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.DeleteCategory(id);
        }
    }
}

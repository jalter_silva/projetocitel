﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Provider.Models;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        [HttpPost]
        public async Task<Produto> InsertProduto([FromBody]Produto produto)
        {
            Provider.Provider provider = new Provider.Provider();

            produto = await provider.InsertProduto(produto);

            return produto;
        }

        [HttpPut]
        public async Task<Produto> Put([FromBody]Produto produto)
        {
            Provider.Provider provider = new Provider.Provider();

            produto = await provider.UpdateProduto(produto);

            return produto;
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete([FromHeader] Produto produto)
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.DeleteProduto(produto);
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<Produto>>> GetCategoria()
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.GetProduto();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Produto>> Get(int id)
        {
            Provider.Provider provider = new Provider.Provider();

            return await provider.GetProduto(id);
        }
    }
}
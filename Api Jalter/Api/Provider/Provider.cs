﻿using Dapper;
using Provider.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    public class Provider
    {
        #region Insert Categoria
        public async Task<Categoria> InsertCategory(Categoria categoria)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    categoria.Id = await conn.ExecuteScalarAsync<int>(@"INSERT INTO categoria(Nome, Status) VALUES(@Nome, @Status); SELECT SCOPE_IDENTITY();",
                        new { categoria.Nome, categoria.Status });
                }

                return categoria;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Update Categoria
        public async Task<Categoria> PutCategory(Categoria categoria)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    categoria.Id = await conn.ExecuteScalarAsync<int>(@"UPDATE Categoria SET Nome = @Nome, Status = @Status WHERE Id = @Id; SELECT Id = @Id",
                        new { categoria.Nome, categoria.Status, categoria.Id });
                }

                return categoria;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Delete Categoria
        public async Task<int> DeleteCategory(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    id = await conn.ExecuteScalarAsync<int>(@"DELETE FROM Categoria WHERE Id = @Id; SELECT Id = @Id",
                        new { id });
                }

                return id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Select Categoria
        public async Task<List<Categoria>> GetCategoria()
        {
            var categorias = new List<Categoria>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    categorias = (await conn.QueryAsync<Categoria>(@"SELECT Id as Id, Nome as Nome, Status as Status FROM Categoria;")).ToList();
                }

                return categorias;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Select Categoria by Id
        public async Task<Categoria> GetCategoria(int id)
        {
            var categorias = new Categoria();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    categorias = (await conn.QueryAsync<Categoria>(@"SELECT Id as Id, Nome as Nome, Status as Status FROM Categoria WHERE id = @id_categoria;",
                        new { id_categoria = id })).ToList().FirstOrDefault();
                }

                return categorias;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Insert Produto
        public async Task<Models.Produto> InsertProduto(Produto produto)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    produto.Id = await conn.ExecuteScalarAsync<int>(@"INSERT INTO Produto(Nome, Status, Quantidade, Id_Categoria) " +
                                                                     "VALUES(@Nome, @Status, @Quantidade, @Id_Categoria); SELECT SCOPE_IDENTITY();",

                        new { produto.Nome, produto.Status, produto.Quantidade, produto.Id_Categoria });
                }

                return produto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Update Produto
        public async Task<Produto> UpdateProduto(Produto produto)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    produto.Id = await conn.ExecuteScalarAsync<int>(@"UPDATE Produto
                                                                        SET Nome = @Nome, 
                                                                            Status = @Status,
                                                                            Quantidade = @Quantidade,
                                                                            Id_Categoria = @Id_Categoria
                                                                        WHERE Id = @Id ;
                                                                        SELECT Id = @Id, Nome = @Nome, Status = @Status,
                                                                        Quantidade = @Quantidade, Id_Categoria = @Id_Categoria",
                        new { produto.Nome, produto.Status, produto.Quantidade, produto.Id_Categoria, produto.Id });
                }

                return produto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Delete Produto
        public async Task<int> DeleteProduto(Produto produto)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    produto.Id = await conn.ExecuteScalarAsync<int>(@"DELETE FROM Produto WHERE Id = @Id; SELECT Id = @Id",
                        new { produto.Id });
                }

                return produto.Id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Select Produto
        public async Task<List<Produto>> GetProduto()
        {
            var produtos = new List<Produto>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    produtos = (await conn.QueryAsync<Produto>(@"SELECT Id as Id, Nome as Nome, Status as Status,
                                                                Quantidade as Quantidade, Id_Categoria as Id_Categoria
                                                                FROM Produto;")).ToList();
                }

                return produtos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Select Produto by Id
        public async Task<Produto> GetProduto(int id)
        {
            var produtos = new Produto();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    produtos = (await conn.QueryAsync<Produto>(@"SELECT Id as Id, Nome as Nome, Status as Status,
                                                                Quantidade as Quantidade, Id_Categoria as Id_Categoria
                                                                FROM Produto WHERE Id = @id_produto;",
                        new { id_produto = id })).ToList().FirstOrDefault();
                }

                return produtos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Select Categoria by Id Categoria [combo box]
        public async Task<List<CategoriaViewModel>> CategoriaCombo()
        {
            var categoriaViewModel = new List<CategoriaViewModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.connectionString))
                {
                    categoriaViewModel = (await conn.QueryAsync<CategoriaViewModel>(@"select a.Id, a.Nome from Categoria a
                                                                                      group by a.Id, a.Nome;"                                                                                     )).ToList();
                }

                return categoriaViewModel;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}

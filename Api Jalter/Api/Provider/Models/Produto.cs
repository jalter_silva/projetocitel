﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Provider.Models
{
    public class Produto
    {
        public int Id{ get; set; }
        public string Nome{ get; set; }
        public int Quantidade{ get; set; }
        public bool Status{ get; set; }
        public int Id_Categoria { get; set; }
    }
}

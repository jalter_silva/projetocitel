﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Provider.Models
{
    public class CategoriaViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}

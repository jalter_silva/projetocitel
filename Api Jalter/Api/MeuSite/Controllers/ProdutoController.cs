﻿using MeuSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MeuSite.Controllers
{
    public class ProdutoController : Controller
    {
        // GET: Produto
        public ActionResult Index()
        {
            try
            {
                
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Produto").Result;
                    List<Produto> prod = response.Content.ReadAsAsync<List<Produto>>().Result;

                    ViewBag.ListaCategoria = CategoriaCombo();
                    return View("GetProduto", prod);
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Detalhes(int id)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Produto/" + id).Result;
                    Produto prod = response.Content.ReadAsAsync<Produto>().Result;

                    return View(prod);
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        // busca no combo box
        public List<CategoriaViewModel> CategoriaCombo()
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Categoria/CategoriaCombo").Result;
                    List<CategoriaViewModel> catViewModel = response.Content.ReadAsAsync<List<CategoriaViewModel>>().Result;

                    return catViewModel;
                }
            }
            catch (Exception t)
            {
                return null;
            }
        }

        [HttpGet] // save
        public ActionResult Create()
        {
            ViewBag.ListaCategoria = CategoriaCombo();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Produto produto)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.PostAsJsonAsync("Produto/", produto).Result;
                    Produto prod = response.Content.ReadAsAsync<Produto>().Result;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(Produto produto)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.PutAsJsonAsync("Produto/", produto).Result;
                    Produto prod = response.Content.ReadAsAsync<Produto>().Result;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.DeleteAsync("Produto/" + id).Result;
                    Produto prod = response.Content.ReadAsAsync<Produto>().Result;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception t)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                ViewBag.ListaCategoria = CategoriaCombo();
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Produto/" + id).Result;
                    Produto prod = response.Content.ReadAsAsync<Produto>().Result;

                    return View(prod);
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }
    }
}
﻿using MeuSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MeuSite.Controllers
{
    public class CategoriaController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Categoria").Result;
                    List<Categoria> cat = response.Content.ReadAsAsync<List<Categoria>>().Result;

                    return View("GetCategoria", cat);
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Detalhes(int id)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Categoria/" + id).Result;
                    Categoria cat = response.Content.ReadAsAsync<Categoria>().Result;

                    return View(cat);
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        [HttpGet] // save
        public ActionResult Create()
        {          
            return View();
        }

        [HttpPost]
        public ActionResult Create(Categoria categoria)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.PostAsJsonAsync("Categoria/", categoria).Result;
                    Categoria cat = response.Content.ReadAsAsync<Categoria>().Result;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.DeleteAsync("Categoria/" + id).Result;
                    Categoria cat = response.Content.ReadAsAsync<Categoria>().Result;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception t)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Edit(Categoria categoria)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.PutAsJsonAsync("Categoria/", categoria).Result;
                    Categoria cat = response.Content.ReadAsAsync<Categoria>().Result;

                    return RedirectToAction("Index");
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                string URL = "https://localhost:44315/api/";
                using (HttpClient request = new HttpClient())
                {
                    request.BaseAddress = new Uri(URL);

                    request.DefaultRequestHeaders.Accept.Clear();
                    request.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = request.GetAsync("Categoria/" + id).Result;
                    Categoria cat = response.Content.ReadAsAsync<Categoria>().Result;

                    return View(cat);
                }
            }
            catch (Exception t)
            {
                return View();
            }
        }

    }
}

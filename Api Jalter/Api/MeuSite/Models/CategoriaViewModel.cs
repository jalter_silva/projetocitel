﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeuSite.Models
{
    public class CategoriaViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}